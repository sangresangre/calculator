<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete'),
				'users'=>array('@'),
			),
// 			array('allow', // allow admin user to perform 'admin' and 'delete' actions
// 				'actions'=>array('admin','delete'),
// 				'users'=>array('admin'),
// 			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		Logger::info('Create action');
		//Yii::info('Create action');
		//Yii::log('Create Action', $level, $category);
		//Yii::log("Action login", 'info', __CLASS__);
		$this->layout='//layouts/column2';
		$email_error ='';
		$model=new User;
		
		$hobbies = $model->getHobbyOptions();
		$gender	 =	$model->getGenderOptions();
		
		 //$component_test = Yii::app()->mycmponent->another_function();
		 $comp_obj= new MyComponent;
		$component_test = $comp_obj->another_function();
		
				
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			
			$model->attributes=$_POST['User'];
			
			
			//echo "hi<pre>".$model->email;
// 			print_r($email_test);
// 			exit;
			
			/*$email_test=$model->checkUniqueEmail($model->email);
			if(count($email_test)==0)
			{
				
				$email_error = "Email New";
			}
			else {
					 $email_error = "Email already exist";
				}
				$this->render('create',array(
						'model'=>$model,
						'hobbies'=>$hobbies,
						'gender'=>$gender,
						'email_error'=>$email_error,
						'component_test'=>$component_test
				));
				exit;
			*/
			if($model->validate())
			{
				
				//var_dump($model->attributes);	
				if(!empty($model->hobbies))
				{
					$model->hobbies = implode(",",$model->hobbies);
				}
				
				$email_test=$model->checkUniqueEmail($model->email);
				if(count($email_test)==0)
				{
					if($model->save())
					{
						$this->redirect(array('view','id'=>$model->user_id));
					}
				}
				else {
					 $email_error = "Email already exist";
				}
				//exit;
			}
			else  
			{
				Logger::error('Login model Error found');
				//$model->getErrors();
			}
			
		}

		$this->render('create',array(
			'model'=>$model,
			'hobbies'=>$hobbies,
			'gender'=>$gender,
			'email_error'=>$email_error,
			'component_test'=>$component_test
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layout='//layouts/column1';
		$email_error ='';
		$model=$this->loadModel($id);
		
		$hobbies = $model->getHobbyOptions();
		$gender	 =	$model->getGenderOptions();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			
			$email_test=$model->checkUniqueEmail($model->email,$id);
// 			print_r($email_test);
// 			exit;

			//var_dump($model->attributes);
			if(!empty($model->hobbies))
			{
				$model->hobbies = implode(",",$model->hobbies);
			}
			if(count($email_test)==0)
			{
				if($model->save())
					$this->redirect(array('view','id'=>$model->user_id));
			}
			else {
				$email_error = "Email already exist";
			}
			
		}

		$this->render('update',array(
			'model'=>$model,
			'hobbies'=>$hobbies,
			'email_error'=>$email_error,
			'gender'=>$gender
				
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
