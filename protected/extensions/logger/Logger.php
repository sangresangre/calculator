<?php
/*class for overriding the log class of yii framework*/
class Logger extends CFileLogRoute
{
 /* overriding the default message format*/
    protected function formatLogMessage($message,$level='info',$category='',$time)
    {
     // check if session is set  
     if(session_id() == '')
   {
       //session has NOT been started
       session_start();
   }
   $session_id = session_id();
        $micro      = sprintf("%06d",($time - floor($time)) * 1000000);
        return "[".$session_id."][".date('Y-m-d H:i:s.'.$micro,$time)."] [$level] [$category] $message\n";
    }
    
    /*log the info level information */
 public static function info($message) {
     Yii::log($message,'info');
    }
    
    /*log the debug level information */
 public static function warning($message) {
     Yii::log($message,'warning');
    }
    
    /*log the debug level information */
 public static function debug($message) {
     Yii::log(json_encode($message),'debug');
    }
    
    /*
     * log the error level information
     * this will send  mail to specified email-id in config file
    */
    public static function error($message) {
     Yii::log($message,'error');
    }
    
 
    
    
}
?>