<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $user_id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $phone_no
 * @property string $hobbies
 * @property string $gender
 * @property integer $status
 * @property string $added_date
 * @property string $modify_date
 * @property string  $PasswordConfirm;
 */
class User extends CActiveRecord
{
	public $PasswordConfirm;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, phone_no, hobbies, gender, status, email', 'required'),
			array('password, PasswordConfirm', 'required','on'=>'insert'),
				//array('verifyPassword','required', 'on'=>'register'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('username, password', 'length', 'max'=>255),
			array('password', 'compare', 'compareAttribute' => 'PasswordConfirm','on'=>'insert'),//working	//,'on'=>'register'
			array('email', 'email'),
			//array('email', 'unique','message'=>'Email already exists!'),
			array('username', 'unique','message'=>'Username already used,Please try another username!'), 
			array('phone_no, gender', 'length', 'max'=>10),
			
			
// 				array( 'password', 'compare' ),
// 				array( 'PasswordConfirm', 'safe' ), 
			// array('password', 'compare', 'compareAttribute'=>'PasswordConfirm', 'on'=>'insert'),
			array('phone_no', 'numerical', 'integerOnly'=>TRUE,'message'=>'Please enter valid Phone no.'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, email,username, password, phone_no, hobbies, gender, status, added_date, modify_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'email' => 'Email',
			'username' => 'Username',
			'password' => 'Password',
			'phone_no' => 'Phone No',
			'hobbies' => 'Hobbies',
			'gender' => 'Gender',
			'status' => 'Status',
			'added_date' => 'Added Date',
			'modify_date' => 'Modify Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('phone_no',$this->phone_no,true);
		$criteria->compare('hobbies',$this->hobbies,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('added_date',$this->added_date,true);
		$criteria->compare('modify_date',$this->modify_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/*
	 * To get the gender option
	 */
	
	public function getGenderOptions() {
		return array(
				'm'=>'Male',
				'f'=>'Female'
		);
	}
	
	/*
	 * To get the Hobby option
	*/
	public function getHobbyOptions() {
		return array(
				'cricket'=>'Cricket',
				'football'=>'Football',
				'reading'=>'Reading'
		);
	}
	
	/*
	 * To chck the uniqueness nature of email
	*/
	public function checkUniqueEmail($email,$id='NULL')
	{
		$criteria=new CDbCriteria;

		$criteria->condition = 'email = :email';
		$criteria->params[':email']=$email;
		
		if(isset($id))
		{
			$criteria->condition .= ' and user_id != :id';
			$criteria->params[':id']=$id;
		}
		
		return $obj=User::model()->find($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function beforeSave()
	{

		$pass = md5($this->password);
		$this->password = $pass;
		
		echo "<BR>onBeforeSave";
		return parent::beforeSave();
	}
	
	public function afterSave()
	{
		echo "<BR>onAfterSave";
		return parent::afterSave();
	}
}
