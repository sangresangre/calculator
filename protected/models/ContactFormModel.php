<?php

/**
 * ContactForm class.
* ContactForm is the data structure for keeping
* contact form data. It is used by the 'contact' action of 'SiteController'.
*/
class ContactFormModel extends CFormModel
{
	public $name;
	public $email;
	public $subject;
	public $body;
	public $verifyCode;
	public $hobbies;
	public $gender;
	

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
				// name, email, subject and body are required
				array('name, email, subject, body, hobbies, gender', 'required'),
				// email has to be a valid email address
				array('email', 'email'),
				// verifyCode needs to be entered correctly
				array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
				'verifyCode'=>'Verification Code',
		);
	}
	public function getHobbyOptions() {
		return array(
				'cricket'=>'Cricket',
				'football'=>'Football',
				'reading'=>'Reading'
		);
	}
	public function getGenderOptions() {
		return array(
				'm'=>'Male',
				'f'=>'Female'
		);
	}
	
}