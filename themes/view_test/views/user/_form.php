<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
//print_r($component_test);
//echo $component_test;
$this->widget('application.widgets.MyWidget',array()); 
?>
<span>In theme</span>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'email').$email_error; ?>
	</div>

	<?php 
// 	echo  "<pre>";
// 	print_r($model);
// 	exit;
	//if($model->isNewRecord)
	//{
	?>
	<!-- div class="row">
		<?php //echo $form->labelEx($model,'password'); ?>
		<?php //echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255)); ?>
		<?php //echo $form->error($model,'password'); ?>
	</div>
	
	<div class="row">
		<?php //echo  $form->labelEx($model, 'PasswordConfirm'); ?>
        <?php //echo  $form->passwordField($model, 'PasswordConfirm', array('size'=>60,'maxlength'=>255)); ?>
		<?php //echo $form->error($model,'PasswordConfirm'); ?>
	</div -->
	<?php 
	//}
	?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'phone_no'); ?>
		<?php echo $form->textField($model,'phone_no',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'phone_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hobbies'); ?>
		<?php //echo $form->textArea($model,'hobbies',array('rows'=>6, 'cols'=>50)); ?>
		
		<?php 
		echo $form->checkBoxList($model,'hobbies',$hobbies);?>
		<?php echo $form->error($model,'hobbies'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gender'); ?>
		<?php 
		/*echo CHtml::radioButton('btn', false, array(
		    'value'=>'1',
		    'id'=>'btnname1',
		    'uncheckValue'=>null
		)); 
		CHtml::radioButton('btn', false, array(
		    'value'=>'2',
		    'id'=>'btnname2',
		    'uncheckValue'=>null
		));*/
/*
	echo $form->radioButton($model, 'gender', array(
		'value'=>1,
		'uncheckValue'=>null
));
echo $form->labelEx($model,'Male');
echo $form->radioButton($model, 'gender', array(
		'value'=>2,
		'uncheckValue'=>null
));
echo $form->labelEx($model,'Female');
*/
 echo $form->radioButtonList($model,'gender', $gender);
	 ?>
		
		<?php //echo $form->textField($model,'gender',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php //echo $form->textField($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', array(1=>'Active',0=>'Inactive'), array('empty'=>'Select Status'));?>
		
		<?php echo $form->error($model,'status'); ?>
	</div>

	<!--  div class="row">
		<?php echo $form->labelEx($model,'added_date'); ?>
		<?php echo $form->textField($model,'added_date'); ?>
		<?php echo $form->error($model,'added_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modify_date'); ?>
		<?php echo $form->textField($model,'modify_date'); ?>
		<?php echo $form->error($model,'modify_date'); ?>
	</div-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->